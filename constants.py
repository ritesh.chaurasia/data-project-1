# Dictinary to fetch short names and color of a team
TEAMS = {"Chennai Super Kings": {"name": "CSK", "color": "#FFFF3C"}, "Delhi Daredevils": {"name": "DD", "color": "#2E0854"},
         "Royal Challengers Bangalore": {"name": "RCB", "color": "#EC1C24"}, "Kolkata Knight Riders": {"name": "kkr", "color": "#2E0854"},
         "Rajasthan Royals": {"name": "RR", "color": "#CBA92B"}, "Kings XI Punjab": {"name": "kxip", "color": "#DCDDDF"},
         "Sunrisers Hyderabad": {"name": "SRH", "color": "#FF822A"}, "Mumbai Indians": {"name": "MI", "color": "#004BA0"}, "Kochi Tuskers Kerala": {"name": "KTK", "color": "#B61314"},
         "Gujarat Lions": {"name": "GL", "color": "#E3263B"},  "Rising Pune Supergiants": {"name": "RPS", "color": "#D11D9B"}}
