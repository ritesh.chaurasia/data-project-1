import unittest

from extra_runs import *


class test_ExtraRuns(unittest.TestCase):
    def test_extra_runs(self):
        input = ("ipl/test_matches.csv", "ipl/test_deliveries.csv")

        data = data_extra_runs(input[0], input[1])
        print(data)
        output = sum(data.values())

        expected_output = 193
        self.assertEqual(output, expected_output)


if __name__ == "__main__":
    unittest.main()
