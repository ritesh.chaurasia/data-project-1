import unittest

from matches_per_year import *


class Test_matches_per_year(unittest.TestCase):
    def test_matches_per_year(self):
        input = "ipl/test_matches.csv"
        output = {"2015": 10, "2016": 11}
        expected_output = count_matches_per_year(input)
        self.assertEqual(output, expected_output)


class Test_sql_matches_per_year(unittest.TestCase):
    def test_sql_matches_per_year(self):
        input = "test_db"
        output = {"2015": 10, "2016": 11}
        expected_output = sql_count_matches_per_year(input)
        self.assertEqual(output, expected_output)


if __name__ == "__main__":
    unittest.main()
