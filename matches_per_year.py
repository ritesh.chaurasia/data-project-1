import csv
import matplotlib.pyplot as plt
from sql_queries import sql_count_matches_per_year

# Create a dictionary with year as keys and wins as values


def count_matches_per_year(match_csv):
    f = open(match_csv)
    reader = csv.DictReader(f)
    matches = {}
    for rows in reader:
        season = rows["season"]
        if season in matches:
            matches[season] += 1
        else:
            matches[season] = 1
    f.close()
    print(matches)
    return matches


def plot_matches_per_year(matches_per_year):
    matches_per_year = sorted(matches_per_year.items())
    season = []
    wins = []
    for year, win in matches_per_year:
        season.append(year)
        wins.append(win)
    plt.bar(season, wins)
    plt.xlabel("Year")
    plt.ylabel("Matches")
    plt.title("Total no. of Matches Played per year")
    plt.savefig("matches_per_year.png")
    plt.show()


def matches_per_year(match_csv):
    # total_matches_per_year = count_matches_per_year(match_csv)
    total_matches_per_year = sql_count_matches_per_year(match_csv)
    plot_matches_per_year(total_matches_per_year)


if __name__ == "__main__":
    matches_per_year("matches_db")
