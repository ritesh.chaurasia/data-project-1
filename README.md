## Data Project - 1

In this project we will derive insights from raw data of IPL by plotting them using Matplotlib. There are 5 tasks performed in this project. Each task has one python file and one .png file which represents it's output.

## Getting Started

Download [IPL Data](https://www.kaggle.com/manasgarg/ipl) from kaggle<br>
Install Matplotlib library<br>


## Tasks Performed

1- Plot the number of matches played per year of all the years in IPL.<br>
>[matches_per_year.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/matches_per_year.py) : Python file<br>
>[matches_per_year.png](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/matches_per_year.png) : Output Plot <br>
>[test_matches_per_year.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/test_matches_per_year.py) : Testing File<br>

2- Plot a stacked bar chart of matches won of all teams over all the years of IPL.<br>
>[total_matches_won_by_teams.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/total_matches_won_by_teams.py) : Python File<br>
>[total_matches_won_by_teams.png](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/total_matches_won_by_teams.png) : Output File<br>
>[test_total_matches_won_by_teams.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/test_total_matches_won_by_teams.py) : Testing File<br>

3- For the year 2016 plot the extra runs conceded per team.<br>
>[extra_runs.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/extra_runs.py) : Python File<br>
>[extra_runs.png](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/extra_runs.py) : Output Plot<br>
>[test_extra_runs.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/test_extra_runs.py) : Testing File <br>

4- For the year 2015 plot the top economical bowlers.<br>
> [top_economical_bowlers.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/top_economical_bowlers.py) : Python File<br>
> [top_economical_bowlers.png](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/top_economical_bowlers.png) : Output Plot<br>
> [test_top_economical_bowlers.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/test_top_economical_bowlers.py) : Testing File <br>

5- Plot the most economical teams in all seasons : Find out the economy of each IPL team in all seasons combined and plot the top teams with minimum economy<br>
>[top_economical_teams.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/top_economical_teams.py) : Python File<br>
>[top_economical_teams.png](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/top_economical_teams.png) : Output Plot<br>
>[test_top_economical_teams.py](https://gitlab.com/ritesh.chaurasia/data-project-1/blob/master/test_top_economical_teams.py) : Testing File <br>
