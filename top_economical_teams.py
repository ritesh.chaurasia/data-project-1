# 10th June 2019
# Plot the top Econoical teams in all ipl seasons

import csv
from operator import itemgetter
import matplotlib.pyplot as plt
from sql_queries import sql_data_economical_teams


def data_economical_teams(deliveries_csv):
    with open(deliveries_csv) as f:
        reader = csv.DictReader(f)
        teams = {}
        for row in reader:
            bowling_team, ball, total_runs = row["bowling_team"], row["ball"], row["total_runs"]
            if bowling_team in teams:
                if int(ball) < 7:  # count only 6 deliveries in an over
                    teams[bowling_team]["deliveries"] += 1
                teams[bowling_team]["runs"] += int(total_runs)
            else:
                teams[bowling_team] = {
                    "deliveries": 1, "runs": int(total_runs)}
        teams = calculate_economy(teams)
        return teams


def calculate_economy(teams):
    for team, values in teams.items():
        overs = values["deliveries"] / 6
        teams[team]["economy"] = round(values["runs"] / overs, 3)
    return teams


def plot_top_economical_teams(teams):
    l = sorted(teams.items(), key=lambda val: val[1]["economy"])
    x = []
    y = []
    for team, values in l[:6]:
        print(team)
        x.append(team[:8])
        y.append(values["economy"])
    plt.yscale("log")
    print(x)
    print(y)
    plt.bar(x, y)
    # plt.yticks([x * 0.5 for x in range(10,20)])
    plt.xlabel("Teams")
    plt.ylabel("Economy")
    plt.title("Top Economical Teams in IPL")
    plt.savefig("top_economical_teams.png")
    plt.show()


def sql_economical_teams(db):
    teams = sql_data_economical_teams(db)
    plot_top_economical_teams(teams)


def top_economical_teams(deliveries_csv):
    teams = data_economical_teams(deliveries_csv)
    plot_top_economical_teams(teams)


if __name__ == "__main__":
    # top_economical_teams("ipl/deliveries.csv")
    sql_economical_teams("matches_db")
