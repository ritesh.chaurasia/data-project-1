# 10th June 2019
# Plot the top Econoical bowlers for the year 2015

import csv
from operator import itemgetter
import matplotlib.pyplot as plt
from sql_queries import sql_data_economical_bowlers


def matches_in_2015(matches_csv):
    match_ids = set()
    with open(matches_csv) as f:
        reader = csv.DictReader(f)

        for row in reader:
            season, id = row["season"], row["id"]
            if season == "2015":
                match_ids.add(id)
    return match_ids


def data_economical_bowlers(matches_csv, deliveries_csv):
    match_ids = matches_in_2015(matches_csv)
    with open(deliveries_csv) as f:
        reader = csv.DictReader(f)
        bowlers = {}
        for row in reader:
            id, bowler, ball, total_runs = row["match_id"], row["bowler"], row["ball"], row["total_runs"]
            if id in match_ids:
                if bowler in bowlers:
                    if int(ball) < 7:  # count only 6 deliveries in an over
                        bowlers[bowler]["deliveries"] += 1
                    bowlers[bowler]["runs"] += int(total_runs)
                else:
                    bowlers[bowler] = {
                        "deliveries": 1, "runs": int(total_runs)}
        bowlers = calculate_economy(bowlers)
        return bowlers


def calculate_economy(bowlers):
    for bowler, values in bowlers.items():
        overs = values["deliveries"] / 6
        bowlers[bowler]["economy"] = round(values["runs"] / overs, 3)
    return bowlers


def plot_top_economical_bowlers(bowlers):
    l = sorted(bowlers.items(), key=lambda val: val[1]["economy"])
    x = []
    y = []
    for bowler, values in l[:6]:
        print(bowler)
        x.append(bowler[:12])
        y.append(values["economy"])
    print(x)
    print(y)
    plt.bar(x, y)
    plt.xlabel("Bowlers")
    plt.ylabel("Economy")
    plt.title("Top Economical Bowlers in IPL 2015")
    plt.savefig("4_top_economical_bowlers.png")
    plt.show()


def sql_top_economical_bowlers(db):
    data = sql_data_economical_bowlers(db)
    plot_top_economical_bowlers(data)


def top_economical_bowlers(matches_csv, deliveries_csv):
    data = data_economical_bowlers(matches_csv, deliveries_csv)
    plot_top_economical_bowlers(data)


if __name__ == "__main__":
    # top_economical_bowlers("ipl/matches.csv", "ipl/deliveries.csv")
    sql_top_economical_bowlers("matches_db")
