# 10th June 2019
# Find extra runs conceded by each team in each year

import csv
import matplotlib.pyplot as plt
import constants
import psycopg2
import psycopg2.extras
from sql_queries import sql_data_extra_runs

TEAMS = constants.TEAMS

# Finds match_id of all matches played in 2019


def matches_in_2016(matches_csv):
    # Using sets for O(1) time complexity
    match_ids = set()
    with open(matches_csv) as f:
        reader = csv.DictReader(f)

        for row in reader:
            if row["season"] == "2016":
                match_ids.add(row["id"])
    return match_ids

# Extracts the data of extra conceded by each team


def data_extra_runs(matches_csv, deliveries_csv):
    match_ids = matches_in_2016(matches_csv)
    extra_runs = {}

    with open(deliveries_csv) as f:
        reader = csv.DictReader(f)
        for row in reader:
            id, bowling_team, extra_run = row["match_id"], row["bowling_team"], row["extra_runs"]
            if id in match_ids:
                if bowling_team in extra_runs:
                    extra_runs[bowling_team] += int(extra_run)
                else:
                    extra_runs[bowling_team] = int(extra_run)

    return extra_runs


def plot_extra_runs(extra_runs):
    print(extra_runs)

    # If items(), keys(), values(),  iteritems(), iterkeys(), and itervalues() are called with no intervening modifications to the dictionary, the lists will directly correspond.
    teams = [TEAMS[team]["name"] for team in extra_runs.keys()]
    runs = extra_runs.values()

    plt.bar(teams, runs, width=0.35)
    plt.xlabel("Teams")
    plt.ylabel("Extra Runs")
    plt.title("Extra runs Conceded by each team in 2016")
    plt.savefig("extra_runs.png")
    plt.show()


def sql_extra_runs(db):
    data = sql_data_extra_runs(db)
    plot_extra_runs(data)


def extra_runs(matches_csv, deliveries_csv):
    data = data_extra_runs(matches_csv, deliveries_csv)
    plot_extra_runs(data)


if __name__ == "__main__":
    # extra_runs("ipl/matches.csv", "ipl/deliveries.csv")
    sql_extra_runs("matches_db")
