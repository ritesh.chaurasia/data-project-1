import unittest

from top_economical_bowlers import *


class TestTopEconomicalBowlers(unittest.TestCase):
    def test_top_economical_bowlers(self):
        expected_output = 2904
        data = data_economical_bowlers(
            "ipl/test_matches.csv", "ipl/test_deliveries.csv")
        output = 0
        for bowler, stats in data.items():
            output += stats["runs"]

        self.assertEqual(output, expected_output)


if __name__ == "__main__":
    unittest.main()
