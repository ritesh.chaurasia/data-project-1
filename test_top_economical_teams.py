import unittest

from top_economical_teams import *


class TestEconomicalTeams(unittest.TestCase):
    def test_top_economical_teams(self):
        expected_output = 6272
        data = data_economical_teams("ipl/test_deliveries.csv")
        output = 0
        for team, stats in data.items():
            output += stats["runs"]
        self.assertEqual(output, expected_output)


if __name__ == "__main__":
    unittest.main()
