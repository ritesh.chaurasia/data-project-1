import psycopg2
import psycopg2.extras


def connect_database(db_input):
    conn = psycopg2.connect(database=db_input)
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    return cur
# SQL implementation for generating data


def sql_count_matches_per_year(db_input):
    cur = connect_database(db_input)
    cur.execute("SELECT * FROM matches")
    rows = cur.fetchall()
    matches = {}
    for row in rows:
        season = row["season"]
        if season in matches:
            matches[season] += 1
        else:
            matches[season] = 1
    return matches


def sql_data_total_macthes_won(db):
    from total_matches_won_by_teams import data_by_team
    cur = connect_database(db)
    cur.execute("""SELECT  COUNT(*) wins,season,winner FROM matches 
                        GROUP BY season,winner ORDER BY season""")
    rows = cur.fetchall()
    wins_per_year = {}

    for row in rows:
        season, wins, winner = row["season"], row["wins"], row["winner"]
        if winner == None:
            continue
        if season not in wins_per_year:
            wins_per_year[season] = {}
        wins_per_year[season][winner] = wins

    years = list(wins_per_year.keys())
    wins_per_year = sorted(wins_per_year.items(),
                           key=lambda year: int(year[0]))
    print(wins_per_year)
    wins_per_year = data_by_team(wins_per_year)
    return (wins_per_year, years)


def sql_data_extra_runs(db):
    cur = connect_database(db)
    cur.execute("""SELECT season, bowling_team, SUM(extra_runs) run FROM 
                    matches JOIN deliveries ON matches.id = deliveries.match_id 
                    GROUP BY season,bowling_team HAVING season=\'2016\'""")
    rows = cur.fetchall()
    extra_runs = {}
    for row in rows:
        extra_runs[row["bowling_team"]] = row["run"]
    return extra_runs


def sql_data_economical_bowlers(db):
    from top_economical_bowlers import calculate_economy
    cur = connect_database(db)
    cur.execute("""SELECT season, bowler, COUNT(*) deliveries, SUM(total_runs) runs 
                        FROM  matches JOIN deliveries ON 
                        matches.id=deliveries.match_id 
                        GROUP BY season, bowler HAVING season=\'2015\'""")
    rows = cur.fetchall()
    bowlers = {}
    for row in rows:
        bowlers[row["bowler"]] = {
            "deliveries": row["deliveries"], "runs": row["runs"]}
    bowlers = calculate_economy(bowlers)
    return bowlers


def sql_data_economical_teams(db):
    from top_economical_teams import calculate_economy
    cur = connect_database(db)
    cur.execute("""SELECT bowling_team, COUNT(*) deliveries, SUM(total_runs) runs 
                    FROM deliveries GROUP BY bowling_team""")
    rows = cur.fetchall()
    teams = {}
    for row in rows:
        teams[row["bowling_team"]] = {
            "deliveries": row["deliveries"], "runs": row["runs"]}
    teams = calculate_economy(teams)
    return teams
