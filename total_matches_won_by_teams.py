import csv
import matplotlib.pyplot as plt
from sql_queries import sql_data_total_macthes_won

# Data Errors: 'Rising Pune Supergiant', 'Rising Pune Supergiants' in winner Column
# "Deccan Chargers" and "Sunrisers Hyderabad" wins are merged and are named jointly as srh
# "Pune Warriors" and "Rising Pune Supergiants" wins are also merged and named jointly as rps

import constants

TEAMS = constants.TEAMS

############################################### RAW DATA TO DATA STRUCTURE TRANSFORMATION ##########################################
# Returns data for plotting


def data_total_matches_won_by_teams(matches_csv):

    # Returns a dictionary of total matches won by each team in each year
    with open(matches_csv) as f:
        matches = csv.DictReader(f)
        wins_per_year = {}
        for match in matches:
            season, winner = match["season"], match["winner"]
            if season in wins_per_year:
                if winner in wins_per_year[season]:
                    wins_per_year[season][winner] += 1
                else:
                    if winner != "":
                        wins_per_year[season][winner] = 1
            else:
                wins_per_year[season] = {}
                wins_per_year[season][winner] = 1

        years = list(wins_per_year.keys())
        wins_per_year = sorted(wins_per_year.items(),
                               key=lambda year: int(year[0]))

        wins_per_year = data_by_team(wins_per_year)
        return (wins_per_year, years)


def data_by_team(wins_per_year):
    team_data = {}
    for year, teams in wins_per_year:
        u_teams = teams

        teams = sorted(teams.items())
        level = 0

        for team, wins in teams:
            if team == "Deccan Chargers":
                team = "Sunrisers Hyderabad"
            if team == "Pune Warriors" or team == "Rising Pune Supergiant":
                team = "Rising Pune Supergiants"

            if team not in team_data:
                team_data[team] = {"wins": [], "bottom": []}
            team_data[team]["wins"].append(wins)
            team_data[team]["bottom"].append(level)
            level += wins

        for team in TEAMS:
            if team == "Pune Warriors" or team == "Rising Pune Supergiant":
                team = "Rising Pune Supergiants"

            if team not in u_teams:
                if team == "Sunrisers Hyderabad":
                    if "Deccan Chargers" in u_teams:
                        continue
                if team == "Rising Pune Supergiants":
                    if "Pune Warriors" in u_teams:
                        continue

                if team not in team_data:
                    team_data[team] = {"wins": [], "bottom": []}
                team_data[team]["wins"].append(0)
                team_data[team]["bottom"].append(0)

    return team_data

    # Plotting the stacked bar chart of total matches won by each team in each year #


def plot_total_matches_won_by_teams(wins_per_year_by_teams, years):
    years.sort()
    for team in wins_per_year_by_teams:
        plt.bar(years, wins_per_year_by_teams[team]["wins"], width=0.35, bottom=wins_per_year_by_teams[team]
                ["bottom"], label=TEAMS[team]["name"], color=TEAMS[team]["color"])

    # Sets the legend value colors for each team. Modified the the no. of columns ncols to 4 to fit horizontally
    plt.legend(ncol=5)
    plt.xlabel("Season")
    plt.ylabel("Matches Won")
    plt.yticks(range(0, 100, 5))
    plt.title("Total Matches Won By Each team in each year")
    # Saves the fig in local directory
    plt.savefig("2_total_matches_won_by_teams.png")
    plt.show()


def sql_total_matches_won(db):
    wins_per_year_by_teams, years = sql_data_total_macthes_won(db)

    plot_total_matches_won_by_teams(wins_per_year_by_teams, years)


def total_matches_won_by_teams(matches_csv):
    wins_per_year_by_teams, years = data_total_matches_won_by_teams(
        matches_csv)

    plot_total_matches_won_by_teams(wins_per_year_by_teams, years)


# total_matches_won_by_teams(input("Give the file path of csv file : "))
if __name__ == "__main__":
    # total_matches_won_by_teams("ipl/matches.csv")
    sql_total_matches_won("matches_db")
