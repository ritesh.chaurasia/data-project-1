import unittest

from total_matches_won_by_teams import *


class Test_total_matches_won_by_teams(unittest.TestCase):
    def test_total_matches_won_by_teams(self):
        input = "ipl/test_matches.csv"
        expected_output = {'Chennai Super Kings': {'wins': [2, 0], 'bottom': [0, 0]},
                           'Sunrisers Hyderabad': {'wins': [0, 0], 'bottom': [0, 0]},
                           'Delhi Daredevils': {'wins': [0, 2], 'bottom': [0, 0]},
                           'Kings XI Punjab': {'wins': [0, 1], 'bottom': [0, 5]},
                           'Kolkata Knight Riders': {'wins': [0, 2], 'bottom': [0, 6]},
                           'Mumbai Indians': {'wins': [4, 1], 'bottom': [2, 8]},
                           'Rajasthan Royals': {'wins': [1, 0], 'bottom': [6, 0]},
                           'Royal Challengers Bangalore': {'wins': [2, 1], 'bottom': [7, 10]},
                           'Kochi Tuskers Kerala': {'wins': [0, 0], 'bottom': [0, 0]},
                           'Gujarat Lions': {'wins': [0, 3], 'bottom': [0, 2]},
                           'Rising Pune Supergiants': {'wins': [0, 1], 'bottom': [0, 9]},
                           }, ["2015", "2016"]

        output = data_total_matches_won_by_teams(input)
        self.assertEqual(output, expected_output)


if __name__ == "__main__":
    unittest.main()
